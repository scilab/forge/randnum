// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

// A sequence of random numbers uniform in [0,1]
seed = 123456;
[ value, seed ] = randnum_shrage ( seed );
[ value, seed ] = randnum_shrage ( seed );
[ value, seed ] = randnum_shrage ( seed );
assert_checktrue ( value > 0 );
assert_checktrue ( value < 1 );
assert_checktrue ( seed >=1 );
assert_checktrue ( seed <=2147483646 );

// Get a dimension 10 vector
seed = 1;
[value,seed] = randnum_shrage(seed,10);
assert_checkequal ( size(value) , [10 1] );
assert_checktrue ( value > 0 );
assert_checktrue ( value < 1 );
assert_checktrue ( seed >=1 );
assert_checktrue ( seed <=2147483646 );

// 
// Reference :
//    "A More Portable Fortran Random Number Generator", 
//    Linus Schrage, ACM Transactions on Mathematical Software, 
//    Volume 5, Number 2, June 1979, pages 132-138.
seed = 1;
[value,seed] = randnum_shrage(seed,10,100);
assert_checkequal ( size(value) , [10 100] );
assert_checktrue ( value > 0 );
assert_checktrue ( value < 1 );
assert_checktrue ( seed >=1 );
assert_checktrue ( seed <=2147483646 );
assert_checkequal ( seed , 522329230 );

// Check the order
seed = 1;
[value1,seed] = randnum_shrage(seed);
[value2,seed] = randnum_shrage(seed);
[value3,seed] = randnum_shrage(seed);
[value4,seed] = randnum_shrage(seed);
[value5,seed] = randnum_shrage(seed);
[value6,seed] = randnum_shrage(seed);
V = [
value1,value2,value3
value4,value5,value6
];
seed = 1;
[value,seed] = randnum_shrage(seed,2,3);
assert_checkequal ( size(value) , [2 3] );
assert_checkequal ( V , value );
