// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

k = randnum_planenumber(31,2);
assert_checkequal(k,7);
//
k = randnum_planenumber(2^16,2);
assert_checkequal(k,362);
