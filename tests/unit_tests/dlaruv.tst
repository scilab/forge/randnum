// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

//
// Check the DLARUV sequence in dimension 1 with real values
//
s = [1 1 1 1];
d = 1;
this = randnum_dlaruvstart ( d , s );
// Terms #1 to #40
[value,this] = randnum_dlaruv (this,40);
// Results out of the fortran program
expected= [...
    0.434158577607338 
    3.280980563838298E-002
    0.395344971497980
    0.620637294444524
    0.516652972245442
    0.181485613287951
    0.530159418686015
    0.261434774375974
    0.341937123969576
    0.428364630778422
    0.864427812328973
    3.995005035409704E-002
    9.785043383614678E-002
    0.120934553435777
    0.849251368201397
    0.189051167408937
    0.857555485814107
    0.747620943461900
    0.515497492921487
    0.814969740182224
    0.986038978614165
    0.748127794632556
    2.903054676061245E-002
    0.868320530162354
    0.484407164067836
    0.159436230025310
    0.466453387264142
    0.289874748448089
    0.260919845706230
    0.743979086378463
    0.675738622821758
    0.836102457270957
    0.378574300758789
    0.147281233408624
    0.904197963333264
    9.854790542039282E-002
    0.143579907737507
    0.617844368016140
    0.637422747498693
    0.240882516504715
    ];
assert_checkalmostequal ( value , expected );


// In dimension 4
s = [1 1 1 1];
d = 4;
this = randnum_dlaruvstart ( d , s );
// Terms #1 to #40
[ value , this ] = randnum_dlaruv (this,10);
assert_checkequal ( size(value) , [10 4] );
assert_checktrue ( value>0 );
assert_checktrue ( value<1 );
