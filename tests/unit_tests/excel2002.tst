// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- JVM NOT MANDATORY -->

//
seed = 1.0;
[value,seed] = randnum_excel2002 ( seed );
assert_checkalmostequal ( seed , 0.2113270000008924398571 );
[value,seed] = randnum_excel2002 ( seed );
assert_checkalmostequal ( seed , 0.6537940087646347819827 );

//
// Check the Excel 2002 sequence in dimension 1
seed = 1;
[value,seed] = randnum_excel2002 ( seed,10 );
expected= [...
    0.2113270000008924398571     
    0.6537940087646347819827     
    0.1222870774781767977402     
    0.1927149131743135512806     
    0.8644892849333700723946     
    0.3605943306283734273165     
    0.6082481012554126209579     
    0.8159294294073333730921     
    0.4542532094210400828160     
    0.4320967240346362814307   
    ];
assert_checkalmostequal ( value , expected );
assert_checkequal ( size(value) , [10 1] );


//
// Check the Excel 2002 sequence in dimension 2
seed = 1;
[value,seed] = randnum_excel2002 ( seed,10,2);
assert_checkequal ( size(value) , [10 2] );
assert_checktrue ( value >= 0 );
assert_checktrue ( value <= 1 );
assert_checkequal ( seed , value(10,2) );

