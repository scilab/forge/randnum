// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [ value , seed ] = randnum_parkmiller ( varargin )
    //  Park and Miller RNG
    //
    // Calling Sequence
    //   [value,seed] = randnum_parkmiller(seed)
    //   [value,seed] = randnum_parkmiller(seed,n)
    //   [value,seed] = randnum_parkmiller(seed,n,d)
    //
    //  Parameters
    //    seed : a 1-by-1 matrix of doubles, integer value, a seed for the random number generator. The seed must be in the set 1,2,...,2147483646
    //    n : 1-by-1 matrix of doubles, integer value, the number of samples (default n=1)
    //    d : 1-by-1 matrix of doubles, integer value, the dimension (default d=1)
    //    value : n-by-d matrix of doubles, a pseudorandom value uniform in (0,1).
    // 
    // Description
    //   This is Park and Miller's "minimal standard" random number generator.
    //
    //   The generator is based on the multiplicative congruential
    //   generator 
    //   
    //   <screen>
    //   s = a * s (mod m) 
    //   </screen>
    //   
    //   with
    //   
    //   <screen>
    //   a = 16807
    //   m = 2^31 - 1 = 2147483647.
    //   </screen>
    //   
    //   The implementation uses Shrage's method to compute the 
    //   product a*s (mod m).
    //
    // Examples
	// seed = 1;
    // [value,seed] = randnum_parkmiller(seed)
    // [value,seed] = randnum_parkmiller(seed)
    // [value,seed] = randnum_parkmiller(seed)
	//
	// // 10 first samples
	// seed = 1
    // [value,seed] = randnum_parkmiller(seed,10)
	//
	// // 10 samples in dimension 2
	// seed = 1
    // [value,seed] = randnum_parkmiller(seed,10,2)
    //
    // // See how randnum_parkmiller and randnum_shrage
    // // produce the same numbers.
    // seedS = 1;
    // seedP = 1;
    // for i = 1 : 10
    // 	[valueS,seedS] = randnum_shrage(seedS);
    // 	[valueP,seedP] = randnum_parkmiller(seedP);
    // 	mprintf("#%d - Shrage = %.7f, Parkmiller = %.7f\n",..
    // 		i,valueS,valueP)
    // end
	//
	// // Plot 500 samples in dimension 2
    // seed = 1;
    // [value,seed] = randnum_parkmiller(seed,500,2);
	// plot(value(:,1),value(:,2),".");
	// xtitle("Park-Miller Random Numbers","X1","X2")
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    //
    // Bibliography
    //    S.K. Park & K.W. Miller, "Random number generators: good ones are hard to find," Comm. ACM 31(10):1192-1201, Oct 1988
    //

    [lhs, rhs] = argn()
    apifun_checkrhs ( "randnum_parkmiller" , rhs , 1:3 )
    apifun_checklhs ( "randnum_parkmiller" , lhs , 2 )
    //
    seed = varargin(1)
    n = apifun_argindefault ( varargin , 2 , 1 )
    d = apifun_argindefault ( varargin , 3 , 1 )
    //
    // Check type
    apifun_checktype ( "randnum_parkmiller" , seed , "seed" , 1 , "constant" )
    apifun_checktype ( "randnum_parkmiller" , n , "n" , 2 , "constant" )
    apifun_checktype ( "randnum_parkmiller" , d , "d" , 3 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "randnum_parkmiller" , seed , "seed" , 1 )
    apifun_checkscalar ( "randnum_parkmiller" , n , "n" , 2 )
    apifun_checkscalar ( "randnum_parkmiller" , d , "d" , 3 )
    //
    // Check content
    apifun_checkflint ( "randnum_parkmiller" , seed , "seed" , 1 )
    apifun_checkrange ( "randnum_parkmiller" , seed , "seed" , 1 , 1 , 2147483646 )
    apifun_checkflint ( "randnum_parkmiller" , n , "n" , 2 )
    apifun_checkgreq ( "randnum_parkmiller" , n , "n" , 2 , 1 )
    apifun_checkflint ( "randnum_parkmiller" , d , "d" , 3 )
    apifun_checkgreq ( "randnum_parkmiller" , d , "d" , 3 , 1 )
    //
    // Proceed...
    a = 16807
    m = 2147483647
	// q = m div a
    q = 127773 
	// r = m mod a
    r = 2836 
    value = zeros(n,d)
    for i = 1:n
        for j = 1:d
            hi = int ( seed / q )
            lo = seed-hi*q // seed mod q
            seed = a * lo - r * hi
            if ( seed < 0 ) then
                seed = seed + m
            end
            value(i,j) = seed / m
        end
    end
endfunction

