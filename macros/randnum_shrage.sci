// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2007 - John Burkardt
// Copyright (C) 1979 - Linus Schrage
//
// This file must be used under the terms of the GNU LGPL license.

function [ value , seed ] = randnum_shrage ( varargin )
    //  A portable random number generator by Schrage.
    //
    // Calling Sequence
    //   [value,seed]=randnum_shrage(seed)
    //   [value,seed]=randnum_shrage(seed,n)
    //   [value,seed]=randnum_shrage(seed,n,d)
    //
    //  Parameters
    //    seed : a 1-by-1 matrix of doubles, integer value, a seed for the random number generator. The seed must be in the set 1,2,...,2147483646. 
    //    n : 1-by-1 matrix of doubles, integer value, the number of samples (default n=1)
    //    d : 1-by-1 matrix of doubles, integer value, the dimension (default d=1)
    //    value : n-by-d matrix of doubles, a pseudorandom value uniform in (0,1).
    // 
    // Description
    //   This is a portable random number generator by Schrage.
    //
    //   The generator is based on the multiplicative congruential
    //   generator 
    //   
    //   <screen>
    //   s = a * s (mod m) 
    //   </screen>
    //   
    //   with
    //   
    //   <screen>
    //   a = 16807
    //   m = 2^31 - 1 = 2147483647.
    //   </screen>
    //   
    //   This generator has period m-1.
    //
    //   The implementation may be slower than Park and Miller's 
    //   "Good ones are hard to find".
    //
    //   Genz uses the seed = 123456.
    //
    // The output <literal>value</literal> is made of non-overlapping 
    // uplets from the generator. 
    // The values are generated in the following order:
    //   
    //   <screen>
    // X = [
    // s(1) s(2) ... s(d)
    // s(d+1) s(d+2) ... s(2*d)
    // ...
    // s(n*(d-1)) s(n*(d-1)+1) ... s(n*d)
    // ]
    //   </screen>
    //
    // where <literal>s(i)</literal> is the i-th seed of the generator.
    //
    // Examples
    // // A sequence of random numbers uniform in [0,1]
    // seed = 123456;
    // [value,seed] = randnum_shrage(seed)
    // [value,seed] = randnum_shrage(seed)
    // [value,seed] = randnum_shrage(seed)
    //
    // // Get 10 samples
    // seed = 1
    // [value,seed] = randnum_shrage(seed,10)
    //
    // // Get 10 samples in dimension 2
    // seed = 1;
    // [value,seed] = randnum_shrage(seed,10,2)
    //
    // // Another sequence
    // seed = 1;
    // [value,seed] = randnum_shrage(seed,10,100);
    // // Must be : "seed = 522329230"
    // mprintf("seed=%d\n",seed)
	//
	// // Plot 500 samples in dimension 2
    // seed = 1;
    // [value,seed] = randnum_shrage(seed,500,2);
	// plot(value(:,1),value(:,2),".");
	// xtitle("Shrage Random Numbers","X1","X2")
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    // Copyright (C) 2007 - John Burkardt (MATLAB version)
    // Copyright (C) 1979 - Linus Schrage
    //
    // Bibliography
    //    "A More Portable Fortran Random Number Generator", Linus Schrage, ACM Transactions on Mathematical Software, Volume 5, Number 2, June 1979, pages 132-138.
    //    "A pseudo-random number generator for the system/360", Lewis, P A W, Goodman, A.S., And Miller, J M . IBM Syst. 3". 8, 2 (1969), 136-146.
    //

    [lhs, rhs] = argn()
    apifun_checkrhs ( "randnum_shrage" , rhs , 1:3 )
    apifun_checklhs ( "randnum_shrage" , lhs , 2 )
    //
    seed = varargin(1)
    n = apifun_argindefault ( varargin , 2 , 1 )
    d = apifun_argindefault ( varargin , 3 , 1 )
    //
    // Check type
    apifun_checktype ( "randnum_shrage" , seed , "seed" , 1 , "constant" )
    apifun_checktype ( "randnum_shrage" , n , "n" , 2 , "constant" )
    apifun_checktype ( "randnum_shrage" , d , "d" , 3 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "randnum_shrage" , seed , "seed" , 1 )
    apifun_checkscalar ( "randnum_shrage" , n , "n" , 2 )
    apifun_checkscalar ( "randnum_shrage" , d , "d" , 3 )
    //
    // Check content
    apifun_checkflint ( "randnum_shrage" , seed , "seed" , 1 )
    apifun_checkrange ( "randnum_shrage" , seed , "seed" , 1 , 1 , 2147483646 )
    apifun_checkflint ( "randnum_shrage" , n , "n" , 2 )
    apifun_checkgreq ( "randnum_shrage" , n , "n" , 2 , 1 )
    apifun_checkflint ( "randnum_shrage" , d , "d" , 3 )
    apifun_checkgreq ( "randnum_shrage" , d , "d" , 3 , 1 )
    //
    // Proceed...

    a = 16807
    b15 = 32768
    b16 = 65536
    p = 2147483647
	value = zeros(n,d)

    for i = 1:n
        for j = 1:d
            xhi = floor ( seed / b16 )
            xalo = ( seed - xhi * b16 ) * a
            leftlo = floor ( xalo / b16 )
            fhi = xhi * a + leftlo
            k = floor ( fhi / b15 )
            seed = ( ( ( xalo - leftlo * b16 ) - p  ) + ( fhi - k * b15 ) * b16 ) + k
            if ( seed < 0 )
                seed = seed + p
            end
            value(i,j) = seed / p
        end
    end
endfunction

