// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the GNU LGPL license.

function [values,indices] = randnum_combineabssumrep(s,n,a)
    // Returns the repeated combinations which absolute value sums to s.
    //
    // Calling Sequence
    // values = randnum_combineabssumrep(s,n,a)
    // [values,indices] = randnum_combineabssumrep(s,n,a)
    // 
    // Parameters
    // s : a 1-by-1 matrix of doubles, integer value, the sum
    // n : a 1-by-1 matrix of doubles, integer value, the number of repetitions
    // a1 : a n1-by-1 matrix of doubles, integer value
    // values : a m-by-n matrix of doubles, where m is the number of matching combinations. On output, we have sum(abs(values),"c")==s.
    // indices : a m-by-n matrix of doubles, where m is the number of matching combinations. The indices corresponding to the arguments.
    //
    // Description
    // Returns all uplets (i1,i2,i3,...) such that 
    //<screen>
    // |a(i1)|+|a(i2)|+|a(i3)|+...=s.
    //</screen>
	//
    // The k-th uplet is :
    //<screen>
    // i1 = indices(k,1)
    // i2 = indices(k,2)
    // i3 = indices(k,3)
    // ...
    //</screen>
	// and the corresponding values are 
    //<screen>
    // a(i1) = values(k,1)
    // a(i2) = values(k,2)
    // a(i3) = values(k,3)
    // ...
    //</screen>
	// for k= 1, 2, ..., m. 
	//
	// Up to 1000 arguments can be combined.
	//
    // Examples
	// // Find all couples (a(i),a(j)) such that |a(i)|+|a(j)| = 3
	// values = randnum_combineabssumrep(3,2,-3:3)
	// S = sum(abs(values),"c")
	// // Find all 3-uplets (a(i),a(j),a(k)) such that |a(i)|+|a(j)|+|a(k)| = 3
	// values = randnum_combineabssumrep(3,3,-3:3)
	// // Find all 4-uplets
	// values = randnum_combineabssumrep(3,4,-3:3)
	//
	// // Get the indices
	// a = -3:3
	// [values,indices] = randnum_combineabssumrep(3,2,a)
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "randnum_combineabssumrep" , rhs , 3 )
    apifun_checklhs ( "randnum_combineabssumrep" , lhs , 1:2 )
    //
    // Check type
    apifun_checktype ( "randnum_combineabssumrep" , s , "s" , 1 , "constant" )
    apifun_checktype ( "randnum_combineabssumrep" , n , "n" , 2 , "constant" )
    apifun_checktype ( "randnum_combineabssumrep" , a , "a" , 3 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "randnum_combineabssumrep" , s , "s" , 1 )
    apifun_checkscalar ( "randnum_combineabssumrep" , n , "n" , 2 )
    apifun_checkvector ( "randnum_combineabssumrep" , a , "a" , 3 )
    //
    // Check content
    apifun_checkflint ( "randnum_combineabssumrep" , s , "s" , 1 )
    apifun_checkgreq  ( "randnum_combineabssumrep" , s , "s" , 1 , 0 )
    apifun_checkflint ( "randnum_combineabssumrep" , n , "n" , 2 )
    apifun_checkgreq  ( "randnum_combineabssumrep" , n , "n" , 2 , 1 )
    apifun_checkflint ( "randnum_combineabssumrep" , a , "a" , 3 )
	//
	// Create argument
	varlist = list(:)
	for i = 1 : n
		varlist(i) = a
	end
	[values,indices] = randnum_combineabssum(s,varlist(:))
endfunction
