// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function this = randnum_dlaruvstart ( d , s )
    // Startup the RNG from LAPACK/DLARUV.
    // 
    // Calling Sequence
    //   rng = randnum_dlaruvstart(d,s)
    //
    // Parameters
    // d : a 1-by-1 matrix of doubles, integer value, the dimension. Must be in the set 1,2,...,128.
    // s : a 4-by-1 or 1-by-4 matrix of doubles, integer value, the seed. s must be in [0,4095] and s(4) must be odd.
    // rng : a data structure.
    //
    // Description
    //   Startup the LAPACK/DLARUV random number generator.
    //
    //  This routine uses a multiplicative congruential method with modulus
    //  2**48 and multiplier 33952834046453 (see Fishman - 1990).
    //
    //  48-bit integers are stored in 4 integer array elements with 12 bits
    //  per element. Hence the routine is portable across machines with
    //  integers of 32 bits or more.
    //
    // Examples
    // // Startup a new sequence in dimension 1
    // s = [1 1 1 1];
    // d = 1;
    // newrng = randnum_dlaruvstart(d,s);
    //
    // // Startup a new sequence in dimension 4
    // s = [1 1 1 1];
    // d = 4;
    // newrng = randnum_dlaruvstart(d,s);
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    //
    // Bibliography
    //   G.S. Fishman, 'Multiplicative congruential random number generators with modulus  2**b: an exhaustive analysis for b = 32 and a partial analysis for b = 48', Math. Comp. 189, pp 331-344, 1990
    // http://www.netlib.org/lapack/explore-html/d5/d46/dlaruv_8f_source.html

    [lhs, rhs] = argn()
    apifun_checkrhs ( "randnum_dlaruvstart" , rhs , 2:2 )
    apifun_checklhs ( "randnum_dlaruvstart" , lhs , 1 )
    //
    // Check type
    apifun_checktype ( "randnum_dlaruvstart" , d , "d" , 1 , "constant" )
    apifun_checktype ( "randnum_dlaruvstart" , s , "s" , 2 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "randnum_dlaruvstart" , d , "d" , 1 )
    apifun_checkvector ( "randnum_dlaruvstart" , s , "s" , 1 )
    //
    // Check content
    apifun_checkflint ( "randnum_dlaruvstart" , d , "d" , 1 )
    apifun_checkrange ( "randnum_dlaruvstart" , d , "d" , 1 , 1 , 128 )
    apifun_checkflint ( "randnum_dlaruvstart" , s , "s" , 2 )
    apifun_checkrange ( "randnum_dlaruvstart" , s , "s" , 2 , 0 , 4095 )
    if (modulo(s(4),2)<>1) then
        error(msprintf(gettext("%s: s(4) must be odd"),"randnum_dlaruvstart"))
    end
    //
    // Proceed...

    this = tlist([
    "T_DLARUV"
    "dlaruvMM"
    "dlaruvLV"
    "dlaruvIPW2"
    "dlaruvR"
    "dlaruv_previous"
    "dlaruv_d"
    ])
    this.dlaruv_d = d
    this.dlaruvMM = zeros(128,4);
    this.dlaruv_previous = s(:);
    this.dlaruvMM( 1, 1:4 ) = [ 494, 322, 2508, 2549 ];
    this.dlaruvMM( 2, 1:4 ) = [ 2637, 789, 3754, 1145];
    this.dlaruvMM( 3, 1:4 ) = [ 255, 1440, 1766, 2253];
    this.dlaruvMM( 4, 1:4 ) = [ 2008, 752, 3572, 305];
    this.dlaruvMM( 5, 1:4 ) = [ 1253, 2859, 2893, 3301];
    this.dlaruvMM( 6, 1:4 ) = [ 3344, 123, 307, 1065];
    this.dlaruvMM( 7, 1:4 ) = [ 4084, 1848, 1297, 3133];
    this.dlaruvMM( 8, 1:4 ) = [ 1739, 643, 3966, 2913];
    this.dlaruvMM( 9, 1:4 ) = [ 3143, 2405, 758, 3285];
    this.dlaruvMM( 10, 1:4 ) = [ 3468, 2638, 2598, 1241];
    this.dlaruvMM( 11, 1:4 ) = [ 688, 2344, 3406, 1197];
    this.dlaruvMM( 12, 1:4 ) = [ 1657, 46, 2922, 3729];
    this.dlaruvMM( 13, 1:4 ) = [ 1238, 3814, 1038, 2501];
    this.dlaruvMM( 14, 1:4 ) = [ 3166, 913, 2934, 1673];
    this.dlaruvMM( 15, 1:4 ) = [ 1292, 3649, 2091, 541];
    this.dlaruvMM( 16, 1:4 ) = [ 3422, 339, 2451, 2753];
    this.dlaruvMM( 17, 1:4 ) = [ 1270, 3808, 1580, 949];
    this.dlaruvMM( 18, 1:4 ) = [ 2016, 822, 1958, 2361];
    this.dlaruvMM( 19, 1:4 ) = [ 154, 2832, 2055, 1165];
    this.dlaruvMM( 20, 1:4 ) = [ 2862, 3078, 1507, 4081];
    this.dlaruvMM( 21, 1:4 ) = [ 697, 3633, 1078, 2725];
    this.dlaruvMM( 22, 1:4 ) = [ 1706, 2970, 3273, 3305];
    this.dlaruvMM( 23, 1:4 ) = [ 491, 637, 17, 3069];
    this.dlaruvMM( 24, 1:4 ) = [ 931, 2249, 854, 3617];
    this.dlaruvMM( 25, 1:4 ) = [ 1444, 2081, 2916, 3733];
    this.dlaruvMM( 26, 1:4 ) = [ 444, 4019, 3971, 409];
    this.dlaruvMM( 27, 1:4 ) = [ 3577, 1478, 2889, 2157];
    this.dlaruvMM( 28, 1:4 ) = [ 3944, 242, 3831, 1361];
    this.dlaruvMM( 29, 1:4 ) = [ 2184, 481, 2621, 3973];
    this.dlaruvMM( 30, 1:4 ) = [ 1661, 2075, 1541, 1865];
    this.dlaruvMM( 31, 1:4 ) = [ 3482, 4058, 893, 2525];
    this.dlaruvMM( 32, 1:4 ) = [ 657, 622, 736, 1409];
    this.dlaruvMM( 33, 1:4 ) = [ 3023, 3376, 3992, 3445];
    this.dlaruvMM( 34, 1:4 ) = [ 3618, 812, 787, 3577];
    this.dlaruvMM( 35, 1:4 ) = [ 1267, 234, 2125, 77];
    this.dlaruvMM( 36, 1:4 ) = [ 1828, 641, 2364, 3761];
    this.dlaruvMM( 37, 1:4 ) = [ 164, 4005, 2460, 2149];
    this.dlaruvMM( 38, 1:4 ) = [ 3798, 1122, 257, 1449];
    this.dlaruvMM( 39, 1:4 ) = [ 3087, 3135, 1574, 3005];
    this.dlaruvMM( 40, 1:4 ) = [ 2400, 2640, 3912, 225];
    this.dlaruvMM( 41, 1:4 ) = [ 2870, 2302, 1216, 85];
    this.dlaruvMM( 42, 1:4 ) = [ 3876, 40, 3248, 3673];
    this.dlaruvMM( 43, 1:4 ) = [ 1905, 1832, 3401, 3117];
    this.dlaruvMM( 44, 1:4 ) = [ 1593, 2247, 2124, 3089];
    this.dlaruvMM( 45, 1:4 ) = [ 1797, 2034, 2762, 1349];
    this.dlaruvMM( 46, 1:4 ) = [ 1234, 2637, 149, 2057];
    this.dlaruvMM( 47, 1:4 ) = [ 3460, 1287, 2245, 413];
    this.dlaruvMM( 48, 1:4 ) = [ 328, 1691, 166, 65];
    this.dlaruvMM( 49, 1:4 ) = [ 2861, 496, 466, 1845];
    this.dlaruvMM( 50, 1:4 ) = [ 1950, 1597, 4018, 697];
    this.dlaruvMM( 51, 1:4 ) = [ 617, 2394, 1399, 3085];
    this.dlaruvMM( 52, 1:4 ) = [ 2070, 2584, 190, 3441];
    this.dlaruvMM( 53, 1:4 ) = [ 3331, 1843, 2879, 1573];
    this.dlaruvMM( 54, 1:4 ) = [ 769, 336, 153, 3689];
    this.dlaruvMM( 55, 1:4 ) = [ 1558, 1472, 2320, 2941];
    this.dlaruvMM( 56, 1:4 ) = [ 2412, 2407, 18, 929];
    this.dlaruvMM( 57, 1:4 ) = [ 2800, 433, 712, 533];
    this.dlaruvMM( 58, 1:4 ) = [ 189, 2096, 2159, 2841];
    this.dlaruvMM( 59, 1:4 ) = [ 287, 1761, 2318, 4077];
    this.dlaruvMM( 60, 1:4 ) = [ 2045, 2810, 2091, 721];
    this.dlaruvMM( 61, 1:4 ) = [ 1227, 566, 3443, 2821];
    this.dlaruvMM( 62, 1:4 ) = [ 2838, 442, 1510, 2249];
    this.dlaruvMM( 63, 1:4 ) = [ 209, 41, 449, 2397];
    this.dlaruvMM( 64, 1:4 ) = [ 2770, 1238, 1956, 2817];
    this.dlaruvMM( 65, 1:4 ) = [ 3654, 1086, 2201, 245];
    this.dlaruvMM( 66, 1:4 ) = [ 3993, 603, 3137, 1913];
    this.dlaruvMM( 67, 1:4 ) = [ 192, 840, 3399, 1997];
    this.dlaruvMM( 68, 1:4 ) = [ 2253, 3168, 1321, 3121];
    this.dlaruvMM( 69, 1:4 ) = [ 3491, 1499, 2271, 997];
    this.dlaruvMM( 70, 1:4 ) = [ 2889, 1084, 3667, 1833];
    this.dlaruvMM( 71, 1:4 ) = [ 2857, 3438, 2703, 2877];
    this.dlaruvMM( 72, 1:4 ) = [ 2094, 2408, 629, 1633];
    this.dlaruvMM( 73, 1:4 ) = [ 1818, 1589, 2365, 981];
    this.dlaruvMM( 74, 1:4 ) = [ 688, 2391, 2431, 2009];
    this.dlaruvMM( 75, 1:4 ) = [ 1407, 288, 1113, 941];
    this.dlaruvMM( 76, 1:4 ) = [ 634, 26, 3922, 2449];
    this.dlaruvMM( 77, 1:4 ) = [ 3231, 512, 2554, 197];
    this.dlaruvMM( 78, 1:4 ) = [ 815, 1456, 184, 2441];
    this.dlaruvMM( 79, 1:4 ) = [ 3524, 171, 2099, 285];
    this.dlaruvMM( 80, 1:4 ) = [ 1914, 1677, 3228, 1473];
    this.dlaruvMM( 81, 1:4 ) = [ 516, 2657, 4012, 2741];
    this.dlaruvMM( 82, 1:4 ) = [ 164, 2270, 1921, 3129];
    this.dlaruvMM( 83, 1:4 ) = [ 303, 2587, 3452, 909];
    this.dlaruvMM( 84, 1:4 ) = [ 2144, 2961, 3901, 2801];
    this.dlaruvMM( 85, 1:4 ) = [ 3480, 1970, 572, 421];
    this.dlaruvMM( 86, 1:4 ) = [ 119, 1817, 3309, 4073];
    this.dlaruvMM( 87, 1:4 ) = [ 3357, 676, 3171, 2813];
    this.dlaruvMM( 88, 1:4 ) = [ 837, 1410, 817, 2337];
    this.dlaruvMM( 89, 1:4 ) = [ 2826, 3723, 3039, 1429];
    this.dlaruvMM( 90, 1:4 ) = [ 2332, 2803, 1696, 1177];
    this.dlaruvMM( 91, 1:4 ) = [ 2089, 3185, 1256, 1901];
    this.dlaruvMM( 92, 1:4 ) = [ 3780, 184, 3715, 81];
    this.dlaruvMM( 93, 1:4 ) = [ 1700, 663, 2077, 1669];
    this.dlaruvMM( 94, 1:4 ) = [ 3712, 499, 3019, 2633];
    this.dlaruvMM( 95, 1:4 ) = [ 150, 3784, 1497, 2269];
    this.dlaruvMM( 96, 1:4 ) = [ 2000, 1631, 1101, 129];
    this.dlaruvMM( 97, 1:4 ) = [ 3375, 1925, 717, 1141];
    this.dlaruvMM( 98, 1:4 ) = [ 1621, 3912, 51, 249];
    this.dlaruvMM( 99, 1:4 ) = [ 3090, 1398, 981, 3917];
    this.dlaruvMM( 100, 1:4 ) = [ 3765, 1349, 1978, 2481];
    this.dlaruvMM( 101, 1:4 ) = [ 1149, 1441, 1813, 3941];
    this.dlaruvMM( 102, 1:4 ) = [ 3146, 2224, 3881, 2217];
    this.dlaruvMM( 103, 1:4 ) = [ 33, 2411, 76, 2749];
    this.dlaruvMM( 104, 1:4 ) = [ 3082, 1907, 3846, 3041];
    this.dlaruvMM( 105, 1:4 ) = [ 2741, 3192, 3694, 1877];
    this.dlaruvMM( 106, 1:4 ) = [ 359, 2786, 1682, 345];
    this.dlaruvMM( 107, 1:4 ) = [ 3316, 382, 124, 2861];
    this.dlaruvMM( 108, 1:4 ) = [ 1749, 37, 1660, 1809];
    this.dlaruvMM( 109, 1:4 ) = [ 185, 759, 3997, 3141];
    this.dlaruvMM( 110, 1:4 ) = [ 2784, 2948, 479, 2825];
    this.dlaruvMM( 111, 1:4 ) = [ 2202, 1862, 1141, 157];
    this.dlaruvMM( 112, 1:4 ) = [ 2199, 3802, 886, 2881];
    this.dlaruvMM( 113, 1:4 ) = [ 1364, 2423, 3514, 3637];
    this.dlaruvMM( 114, 1:4 ) = [ 1244, 2051, 1301, 1465];
    this.dlaruvMM( 115, 1:4 ) = [ 2020, 2295, 3604, 2829];
    this.dlaruvMM( 116, 1:4 ) = [ 3160, 1332, 1888, 2161];
    this.dlaruvMM( 117, 1:4 ) = [ 2785, 1832, 1836, 3365];
    this.dlaruvMM( 118, 1:4 ) = [ 2772, 2405, 1990, 361];
    this.dlaruvMM( 119, 1:4 ) = [ 1217, 3638, 2058, 2685];
    this.dlaruvMM( 120, 1:4 ) = [ 1822, 3661, 692, 3745];
    this.dlaruvMM( 121, 1:4 ) = [ 1245, 327, 1194, 2325];
    this.dlaruvMM( 122, 1:4 ) = [ 2252, 3660, 20, 3609];
    this.dlaruvMM( 123, 1:4 ) = [ 3904, 716, 3285, 3821];
    this.dlaruvMM( 124, 1:4 ) = [ 2774, 1842, 2046, 3537];
    this.dlaruvMM( 125, 1:4 ) = [ 997, 3987, 2107, 517];
    this.dlaruvMM( 126, 1:4 ) = [ 2573, 1368, 3508, 3017];
    this.dlaruvMM( 127, 1:4 ) = [ 1148, 1848, 3525, 2141];
    this.dlaruvMM( 128, 1:4 ) = [ 545, 2366, 3801, 1537];
    this.dlaruvLV = 128;
    this.dlaruvIPW2 = 4096;
    this.dlaruvR = 1.0 / this.dlaruvIPW2;
endfunction
