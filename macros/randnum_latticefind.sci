// Copyright (C) 2012 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [planesnb,uplets] = randnum_latticefind(varargin)
    // Compute the lattice coefficients of a LCG
    // 
    // Calling Sequence
    //   planesnb = randnum_latticefind(a,m,d,kmax)
    //   planesnb = randnum_latticefind(a,m,d,kmax,stopatfirst)
    //   planesnb = randnum_latticefind(a,m,d,kmax,stopatfirst,outfun)
    //   [planesnb,uplets] = randnum_latticefind(...)
    //
    // Parameters
    // a : a 1-by-1 matrix of doubles, integer value, positive, the multiplier of the LCG
    // m : a 1-by-1 matrix of doubles, integer value, positive, the modulo of the LCG
    // d : a 1-by-1 matrix of doubles, integer value, positive, the dimension
    // kmax : a 1-by-1 matrix of doubles, integer value, positive, the maximum number of planes
    // stopatfirst : a 1-by-1 matrix of booleans, set stopatfirst=%f to compute all possible combinations (default stopatfirst=%t)
    // outfun : a function or a list, an output function (default outfun=[])
    // planesnb : a m-by-1 matrix of doubles, integer value, the number of planes of each lattice, where m is the number of lattices
    // uplets : a m-by-d matrix of doubles, integer value, the coefficients found for each lattice, where m is the number of lattices
    //
    // Description
    // Compute the multi-dimensional lattice coefficients of a linear congruential generator 
    // with multiplier a and modulo m. 
    // This type of random number generator is defined by the 
    // recurrence :
    //
    // <screen>
    // s = a*s (mod m)
    // </screen>
    //
    // More precisely, search (c1,c2,c3,...,cd) such that 
    //
    // <screen>
    // c1 + c2*a + c3*a*a + ... + cd*a^(d-1)= 0 (mod m)
    // </screen>
    //
    // The i-th lattice <literal>(c1,c2,...,cd)</literal> is stored 
    // into <literal>uplets(i,1:d)</literal>, for i=1,2,...,m, 
    // where <literal>uplets(i,1)</literal> is the coefficient of the dimension #1, 
    // <literal>uplets(i,2)</literal> is the coefficient of the dimension #2, and 
    // <literal>uplets(i,d)</literal> is the coefficient of the dimension #d.
    //
    // The number of lines in the i-th lattice is <literal>planesnb(i)</literal>, which 
    // satisfies the equality 
    //
    // <screen>
    // planesnb(i) = sum(abs(uplets(i,1:d)))
    // </screen>
    //
    // The <literal>planesnb</literal> matrix is sorted in increasing order.
    //
    // The <literal>outfun</literal> argument is a callback 
    // which can be used to print message during the search.
    // Its calling sequence is 
    //
    // <screen>
    // outfun(step,k,c)
    // </screen>
    //
    // where step is "newk" or "lattice", k is the number of hyperplanes, 
    // and c is a 1-by-d matrix of doubles. 
    // The step is "newk" each time a new value of k is explored, 
    // and is "lattice" each time a new uplet has been discovered.
    // If step=="newk", then c is [].
    // If step=="lattice", then c contains the coefficients
    // of the associated lattice, i.e. corresponds to 
    // the i-th row of uplets.
    //
    // It may happen that the output function requires 
    // extra arguments.
    // In this case, the <literal>outfun</literal can be 
    // the list (f,a1,a2,...) where f is a function and 
    // a1, a2, ... are extra arguments, which are automatically 
    // appended to the calling sequence :
    //
    // <screen>
    // f(step,k,c,a1,a2,...)
    // </screen>
    //
    // Examples
    // // Find the uplets (c1,c2) which has minimum number of lines and 
    // // plots the lines for a=3, m=31
    // a = 3;
    // m = 31;
    // [planesnb,uplets] = randnum_latticefind(a,m,2,20)
    // // Plots the lines for a=3, m=31 
    // // and the couple (c1=3,c2=-1)
    // scf();
    // randnum_lcgplot(a,0,m);
    // randnum_plotlines(uplets(1,1),uplets(1,2));
    //
    // // Find the uplets (c1,c2) with increasing number of lines
    // a = 3;
    // m = 31;
    // [planesnb,uplets] = randnum_latticefind(a,m,2,20,%f)
    // 
    // // Try the multiplier a=11
    // a = 11;
    // m = 31;
    // [planesnb,uplets] = randnum_latticefind(a,m,2,10)
    // 
    // // Compute the minimum number of lines 
    // // covering all points.
    // a = 12;
    // m = 31;
    // [planesnb,uplets] = randnum_latticefind(a,m,2,20)
    // // Plot the couple (c1,c2) which has the minimum 
    // // number of lines
    // scf();
    // randnum_lcgplot(a,0,m);
    // randnum_plotlines(uplets(1,1),uplets(1,2));
    // 
    // // Try all primitive roots modulo 31
    // m = 31;
    // uppernb = randnum_planenumber(m,2)';
    // mprintf("Maximum number of planes:%d\n",uppernb);
    // pr = number_primitiveroot(m,%inf)
    // for a = pr
    //     [planesnb,uplets] = randnum_latticefind(a,m,2,40);
    //     // ratio<=1, ratio==1 is good, ratio small is bad.
    //     ratio = planesnb/uppernb;
    //     mprintf("a=%d, number of lines=%d, ratio=%.2f\n",a,planesnb,ratio);
    //     if (%f) then
    //     scf();
    //     randnum_lcgplot(a,0,m);
    //     randnum_plotlines(uplets(1,1),uplets(1,2));
    //     end
    // end
    // 
    // // Try all primitive roots modulo 47
    // m = 47;
    // pr = number_primitiveroot(m,%inf)'
    // uppernb = randnum_planenumber(m,2);
    // mprintf("Upper number of planes:%d\n",uppernb);
    // for a = pr
    //     [planesnb,uplets] = randnum_latticefind(a,m,2,40);
    //     ratio = planesnb/uppernb;
    //     mprintf("a=%d, number of lines=%d, ratio=%.2f\n",a,planesnb,ratio);
    //     if (%f) then
    //     scf();
    //     randnum_lcgplot(a,0,m);
    //     randnum_plotlines(uplets(1,1),uplets(1,2));
    //     end
    // end
    //
    // // Try the multiplier a=11 in dimension 3
    // a = 11;
    // m = 31;
    // [planesnb,uplets] = randnum_latticefind(a,m,3,10)
    // modulo(uplets(1)+uplets(2)*a+uplets(3)*a^2,m)
    //
    // // See some primitive roots modulo 97 which 
    // // have the maximum number of lines in dimension 2. 
    // m = 97;
    // pr = [13 15 21 37 60 76 82 84];
    // for a = pr 
    // mprintf("a=%d\n",a)
    // for d = 2 : 5
    // 	uppernb = randnum_planenumber(m,d)';
    // 	planesnb = randnum_latticefind(a,m,d,20);
    // 	ratio = planesnb/uppernb;
    // 	mprintf("    d=%d, upper=%d, actual=%d, ratio=%.2f\n",..
    // 	d,uppernb,planesnb,ratio);
    // end
    // end
    //
    // // Try "RANDU" (the "infamous") in dimension 3.
    // a = 65539;
    // m = 2^31;
    // [planesnb,uplets] = randnum_latticefind(a,m,3,20)
    //
    // // Print intermediate messages
    // function myoutfun(step,k,c)
    //     if (step=="newk") then
    //     	mprintf("%s: k=%d\n",..
    //     	step,k)
    //     else
    //     	mprintf("%s: k=%d, c=%s\n",..
    //     	step,k,strcat(string(c)," "))
    //     end
    // endfunction
    // m=31;
    // a = 12;
    // [planesnb,uplets] = randnum_latticefind(a,m,2,%inf,[],myoutfun)
    //
    // Authors
    // Copyright (C) 2012 - Michael Baudin
    //
    // Bibliography
    // Random numbers fall mainly in the planes, G. Marsaglia, Proc National Academy of Sciences, 61, 25-28, 1968

    [lhs, rhs] = argn()
    apifun_checkrhs ( "randnum_latticefind" , rhs , 4:6 )
    apifun_checklhs ( "randnum_latticefind" , lhs , 0:2 )
    //
    a = varargin(1)
    m = varargin(2)
    d = varargin(3)
    kmax = varargin(4)
    stopatfirst = apifun_argindefault ( varargin , 5 , %t )
    __outfun__ = apifun_argindefault ( varargin , 6 , [] )
    //
    // Check type
    apifun_checktype ( "randnum_latticefind" , a , "a" , 1 , "constant" )
    apifun_checktype ( "randnum_latticefind" , m , "m" , 2 , "constant" )
    apifun_checktype ( "randnum_latticefind" , d , "d" , 3 , "constant" )
    apifun_checktype ( "randnum_latticefind" , kmax , "kmax" , 4 , "constant" )
    apifun_checktype ( "randnum_latticefind" , stopatfirst , "stopatfirst" , 5 , "boolean" )
    if ( __outfun__<>[] ) then
        apifun_checktype ( "randnum_latticefind" , __outfun__ , "__outfun__" , 6 , ["function" "list"] )
    end
    //
    // Check size
    apifun_checkscalar ( "randnum_latticefind" , a , "a" , 1 )
    apifun_checkscalar ( "randnum_latticefind" , m , "m" , 2 )
    apifun_checkscalar ( "randnum_latticefind" , d , "d" , 3 )
    apifun_checkscalar ( "randnum_latticefind" , kmax , "kmax" , 4 )
    apifun_checkscalar ( "randnum_latticefind" , stopatfirst , "stopatfirst" , 5 )
    //
    // Check content
    apifun_checkflint ( "randnum_latticefind" , a , "a" , 1 )
    apifun_checkgreq ( "randnum_latticefind" , a , "a" , 1 , 1 )
    apifun_checkflint ( "randnum_latticefind" , m , "m" , 2 )
    apifun_checkgreq ( "randnum_latticefind" , m , "m" , 2 , 1 )
    apifun_checkflint ( "randnum_latticefind" , d , "d" , 3 )
    apifun_checkgreq ( "randnum_latticefind" , d , "d" , 3 , 2 )
    apifun_checkflint ( "randnum_latticefind" , kmax , "kmax" , 4 )
    apifun_checkgreq ( "randnum_latticefind" , kmax , "kmax" , 4 , 1 )
    //
    // Prepare the callback
    if ( __outfun__<>[] ) then
        if (typeof(__outfun__)=="function") then
            __outfun__f = __outfun__
            __outfun__args = list()
        else
            __outfun__f = __outfun__(1)
            __outfun__args = __outfun__(2:$)
        end
    end
    //
    // Pre-compute powers of a
    apow = []
    for i = 0 : d-1
        apow(i+1) = number_powermod ( a, i, m )
    end
    //
    planesnb = []
    uplets = [];
    nc = 1;
    found = %f;
    for k = 1 : kmax
        if ( __outfun__<>[] ) then
            __outfun__f("newk",k,[],__outfun__args(:))
        end
        values = randnum_combineabssumk(k,k,d)
        nbcomb = size(values,"r")
        for i = 1 : nbcomb
            if (values(i,1)==0) then
                continue
            end
            if (values(i,$)==0) then
                continue
            end
            s = sum(values(i,:) .* apow')
            r = pmodulo(s,m);
            if (r==0) then
                nl = sum(abs(values(i,:)));
                // See if this line is new
                f = nl./planesnb;
                fracpart = f - floor(f);
                isdiv = find(fracpart==0);
                if (isdiv==[]) then
                    planesnb(nc) = nl
                    uplets(nc,:)= values(i,:);
                    if ( __outfun__<>[] ) then
                        __outfun__f("lattice",k,values(i,:),__outfun__args(:))
                    end
                    nc = nc+1;
                    if (stopatfirst) then
                        found = %t
                        break
                    end
                end
            end
        end
        if (found) then
            break
        end
    end
endfunction

