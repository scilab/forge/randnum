Randnum toolbox

Purpose
-------

The goal of this toolbox is to provide several random number generators 
algorithms based on macros. 

The goal of this toolbox is *not* to serve the production of 
studies based on the random numbers produced.
Instead, the goal is to see how the random number generators have 
been implemented in the past.

The "number" toolbox can be used to try various LCG, based on various 
primitive roots modulo m.
In this case, the "number_primitiveroot" function can help 
to compute primitive roots modulo m. 
Therefore, the "number" toolbox can help to compute 
multipliers of linear congruential generators and to 
analyse their properties.

Features
--------

 * randnum_dlaruv � Generate uniform random numbers as in LAPACK/DLARUV.
 * randnum_dlaruvstart � Startup the RNG from LAPACK/DLARUV.
 * randnum_excel2002 � Random numbers as in Excel 2002.
 * randnum_parkmiller � Park and Miller RNG
 * randnum_shrage � A portable random number generator by Schrage.

Lattice

 * randnum_latticefind � Compute the lattice coefficients of a LCG
 * randnum_lcgplot � Make a 2D plot random numbers from a LCG
 * randnum_planenumber � Upper bound for the number of planes for d tuples.
 * randnum_plotlines � Plot lines with given coefficients

Dependencies
------------

 * This module depends on the assert module.
 * This module depends on the apifun module.
 * This module depends on the specfun module (>=v0.2).
 * This module depends on the number module (>=v1.3).

TODO
-------
 * randnum_latticefind : add a flag (default = %t) to stop when the first is found.
 * Create unit tests for randnum_latticefind, etc...

Forge
-----

http://forge.scilab.org/index.php/p/randnum/

ATOMS
-------

http://atoms.scilab.org/toolboxes/randnum

Authors
-------

 * Copyright (C) 2008-2009 - INRIA - Michael Baudin
 * Copyright (C) 2012 - Michael Baudin

Licence
-------

This toolbox is released under the CeCILL_V2 licence :

http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

Bibliography
------------

"The Art of Computer Programming", Donald Knuth, Volume 2,
Seminumerical Algorithms.

G.S. Fishman, 'Multiplicative congruential random number generators 
with modulus 2**b: an exhaustive analysis for b = 32 and a 
partial analysis for b = 48', Math. Comp. 189, pp 331-344, 1990

Random Number Generation and Monte Carlo Methods, Second Edition, 
by James E. Gentle, Springer-Verlag, 2003.

S.K. Park & K.W. Miller, "Random number generators: good ones 
are hard to find," Comm. ACM 31(10):1192-1201, Oct 1988

"A More Portable Fortran Random Number Generator", Linus Schrage, 
ACM Transactions on Mathematical Software, Volume 5, Number 2, 
June 1979, pages 132-138.

"A pseudo-random number generator for the system/360", Lewis, 
P A W, Goodman, A.S., And Miller, J M . IBM Syst. 3". 8, 2 (1969), 136-146.

Random numbers fall mainly in the planes, G. Marsaglia, 
Proc National Academy of Sciences, 61, 25-28, 1968
