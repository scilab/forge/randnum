<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from randnum_latticefind.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="randnum_latticefind" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>randnum_latticefind</refname><refpurpose>Compute the lattice coefficients of a LCG</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   planesnb = randnum_latticefind(a,m,d,kmax)
   planesnb = randnum_latticefind(a,m,d,kmax,stopatfirst)
   planesnb = randnum_latticefind(a,m,d,kmax,stopatfirst,outfun)
   [planesnb,uplets] = randnum_latticefind(...)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>a :</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value, positive, the multiplier of the LCG</para></listitem></varlistentry>
   <varlistentry><term>m :</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value, positive, the modulo of the LCG</para></listitem></varlistentry>
   <varlistentry><term>d :</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value, positive, the dimension</para></listitem></varlistentry>
   <varlistentry><term>kmax :</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value, positive, the maximum number of planes</para></listitem></varlistentry>
   <varlistentry><term>stopatfirst :</term>
      <listitem><para> a 1-by-1 matrix of booleans, set stopatfirst=%f to compute all possible combinations (default stopatfirst=%t)</para></listitem></varlistentry>
   <varlistentry><term>outfun :</term>
      <listitem><para> a function or a list, an output function (default outfun=[])</para></listitem></varlistentry>
   <varlistentry><term>planesnb :</term>
      <listitem><para> a m-by-1 matrix of doubles, integer value, the number of planes of each lattice, where m is the number of lattices</para></listitem></varlistentry>
   <varlistentry><term>uplets :</term>
      <listitem><para> a m-by-d matrix of doubles, integer value, the coefficients found for each lattice, where m is the number of lattices</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Compute the multi-dimensional lattice coefficients of a linear congruential generator
with multiplier a and modulo m.
This type of random number generator is defined by the
recurrence :
   </para>
   <para>
<screen>
s = a*s (mod m)
</screen>
   </para>
   <para>
More precisely, search (c1,c2,c3,...,cd) such that
   </para>
   <para>
<screen>
c1 + c2*a + c3*a*a + ... + cd*a^(d-1)= 0 (mod m)
</screen>
   </para>
   <para>
The i-th lattice <literal>(c1,c2,...,cd)</literal> is stored
into <literal>uplets(i,1:d)</literal>, for i=1,2,...,m,
where <literal>uplets(i,1)</literal> is the coefficient of the dimension #1,
<literal>uplets(i,2)</literal> is the coefficient of the dimension #2, and
<literal>uplets(i,d)</literal> is the coefficient of the dimension #d.
   </para>
   <para>
The number of lines in the i-th lattice is <literal>planesnb(i)</literal>, which
satisfies the equality
   </para>
   <para>
<screen>
planesnb(i) = sum(abs(uplets(i,1:d)))
</screen>
   </para>
   <para>
The <literal>planesnb</literal> matrix is sorted in increasing order.
   </para>
   <para>
The <literal>outfun</literal> argument is a callback
which can be used to print message during the search.
Its calling sequence is
   </para>
   <para>
<screen>
outfun(step,k,c)
</screen>
   </para>
   <para>
where step is "newk" or "lattice", k is the number of hyperplanes,
and c is a 1-by-d matrix of doubles.
The step is "newk" each time a new value of k is explored,
and is "lattice" each time a new uplet has been discovered.
If step=="newk", then c is [].
If step=="lattice", then c contains the coefficients
of the associated lattice, i.e. corresponds to
the i-th row of uplets.
   </para>
   <para>
It may happen that the output function requires
extra arguments.
In this case, the <literal>outfun</literal can be
the list (f,a1,a2,...) where f is a function and
a1, a2, ... are extra arguments, which are automatically
appended to the calling sequence :
   </para>
   <para>
<screen>
f(step,k,c,a1,a2,...)
</screen>
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Find the uplets (c1,c2) which has minimum number of lines and
// plots the lines for a=3, m=31
a = 3;
m = 31;
[planesnb,uplets] = randnum_latticefind(a,m,2,20)
// Plots the lines for a=3, m=31
// and the couple (c1=3,c2=-1)
scf();
randnum_lcgplot(a,0,m);
randnum_plotlines(uplets(1,1),uplets(1,2));

// Find the uplets (c1,c2) with increasing number of lines
a = 3;
m = 31;
[planesnb,uplets] = randnum_latticefind(a,m,2,20,%f)

// Try the multiplier a=11
a = 11;
m = 31;
[planesnb,uplets] = randnum_latticefind(a,m,2,10)

// Compute the minimum number of lines
// covering all points.
a = 12;
m = 31;
[planesnb,uplets] = randnum_latticefind(a,m,2,20)
// Plot the couple (c1,c2) which has the minimum
// number of lines
scf();
randnum_lcgplot(a,0,m);
randnum_plotlines(uplets(1,1),uplets(1,2));

// Try all primitive roots modulo 31
m = 31;
uppernb = randnum_planenumber(m,2)';
mprintf("Maximum number of planes:%d\n",uppernb);
pr = number_primitiveroot(m,%inf)
for a = pr
[planesnb,uplets] = randnum_latticefind(a,m,2,40);
// ratio<=1, ratio==1 is good, ratio small is bad.
ratio = planesnb/uppernb;
mprintf("a=%d, number of lines=%d, ratio=%.2f\n",a,planesnb,ratio);
if (%f) then
scf();
randnum_lcgplot(a,0,m);
randnum_plotlines(uplets(1,1),uplets(1,2));
end
end

// Try all primitive roots modulo 47
m = 47;
pr = number_primitiveroot(m,%inf)'
uppernb = randnum_planenumber(m,2);
mprintf("Upper number of planes:%d\n",uppernb);
for a = pr
[planesnb,uplets] = randnum_latticefind(a,m,2,40);
ratio = planesnb/uppernb;
mprintf("a=%d, number of lines=%d, ratio=%.2f\n",a,planesnb,ratio);
if (%f) then
scf();
randnum_lcgplot(a,0,m);
randnum_plotlines(uplets(1,1),uplets(1,2));
end
end

// Try the multiplier a=11 in dimension 3
a = 11;
m = 31;
[planesnb,uplets] = randnum_latticefind(a,m,3,10)
modulo(uplets(1)+uplets(2)*a+uplets(3)*a^2,m)

// See some primitive roots modulo 97 which
// have the maximum number of lines in dimension 2.
m = 97;
pr = [13 15 21 37 60 76 82 84];
for a = pr
mprintf("a=%d\n",a)
for d = 2 : 5
uppernb = randnum_planenumber(m,d)';
planesnb = randnum_latticefind(a,m,d,20);
ratio = planesnb/uppernb;
mprintf("    d=%d, upper=%d, actual=%d, ratio=%.2f\n",..
d,uppernb,planesnb,ratio);
end
end

// Try "RANDU" (the "infamous") in dimension 3.
a = 65539;
m = 2^31;
[planesnb,uplets] = randnum_latticefind(a,m,3,20)

// Print intermediate messages
function myoutfun(step,k,c)
if (step=="newk") then
mprintf("%s: k=%d\n",..
step,k)
else
mprintf("%s: k=%d, c=%s\n",..
step,k,strcat(string(c)," "))
end
endfunction
m=31;
a = 12;
[planesnb,uplets] = randnum_latticefind(a,m,2,%inf,[],myoutfun)

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2012 - Michael Baudin</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>Random numbers fall mainly in the planes, G. Marsaglia, Proc National Academy of Sciences, 61, 25-28, 1968</para>
</refsection>
</refentry>
