<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from randnum_lcg.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="randnum_lcg" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>randnum_lcg</refname><refpurpose>Random numbers from a linear congruential generator</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [value,seed] = randnum_lcg(a,b,m,seed)
   [value,seed] = randnum_lcg(a,b,m,seed,n)
   [value,seed] = randnum_lcg(a,b,m,seed,n,d)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>a :</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value, positive, the multiplier of the LCG</para></listitem></varlistentry>
   <varlistentry><term>b :</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value, positive, the shift of the LCG</para></listitem></varlistentry>
   <varlistentry><term>m :</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value, positive, the modulo of the LCG</para></listitem></varlistentry>
   <varlistentry><term>seed :</term>
      <listitem><para> a 1-by-1 matrix of doubles, integer value, a seed for the random number generator. The seed must be in the set 0,1,2,...,m-1.</para></listitem></varlistentry>
   <varlistentry><term>n :</term>
      <listitem><para> 1-by-1 matrix of doubles, integer value, the number of samples (default n=1)</para></listitem></varlistentry>
   <varlistentry><term>d :</term>
      <listitem><para> 1-by-1 matrix of doubles, integer value, the dimension (default d=1)</para></listitem></varlistentry>
   <varlistentry><term>value :</term>
      <listitem><para> n-by-d matrix of doubles, a pseudorandom value uniform in (0,1).</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Computes values from a linear congruential generator defined by the
recurrence :
   </para>
   <para>
<screen>
s = a*s+b (mod m)
</screen>
   </para>
   <para>
The examples below are based on the book by J.E. Gentle,
"Random Number Generation and Monte Carlo Methods",
and more precisely, Chapter 1. "Simulating Random Numbers
from a Uniform Distribution".
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Numbers from x=3*x (mod 31)
// 3 is a primitive root modulo 31.
a = 3
b = 0
m = 31
seed = 1
[value,seed] = randnum_lcg(a,b,m,seed)
[value,seed] = randnum_lcg(a,b,m,seed)
// Check that 3 is a primitive root modulo 31 :
// number_primitiveroot(31,%inf)

// Compute all the values from this LCG.
a = 3
b = 0
m = 31
seed = 9;
[U,seed] = randnum_lcg(a,b,m,seed,m-1)
// Compute the autocorrelation coefficient.
randnum_autocorr(U,[0 1 2 3 4 5])
scf();
plot((0:5),r,"bo-");
xtitle("Autocorrelation of MCG with m=31 and a=3",..
"Lag","Autocorrelation");

// Reference:
// Random Number Generation and Monte Carlo Methods,
// by Jame Gentle
// Figure 1.3: "Pairs of successive overlapping
// numbers from x=3*x (mod 31)
h = scf();
plot([U(1:30)],[U(2:30);U(1)],"bo")
xtitle("Overlapping pairs from x=3*x (mod 31)",..
"x(i)","x(i+1)")

// Plot of non-overlapping pairs
a = 3;
b = 0;
m = 31;
seed = 9;
[value,seed] = randnum_lcg(a,b,m,seed,m-1,2);
plot(value(:,1),value(:,2),".");
xtitle("Non overlapping pairs from x=3*x (mod 31)",..
"X1","X2")

// Plot overlapping triplets in 3D
a = 11;
m = 31;
d = 1;
seed = 1;
b = 0;
[value,seed] = randnum_lcg(a,b,m,seed,m-1);
V1 = value(1:m-3);
V2 = value(2:m-2);
V3 = value(3:m-1);
h = scf();
param3d(V1,V2,V3);
h.children.children.mark_mode="on";
h.children.children.line_mode="off";
h.children.children.mark_size=1;

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Copyright (C) 2012 - Michael Baudin</member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <para>Random Number Generation and Monte Carlo Methods, Second Edition, by James E. Gentle, Springer-Verlag, 2003.</para>
</refsection>
</refentry>
